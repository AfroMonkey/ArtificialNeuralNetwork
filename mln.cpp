#include "mln.h"

#include <iostream>

MLN::MLN(const std::vector<size_t> &architecture, const std::vector<ActivationFunction> &activation_functions, const std::vector<ActivationFunction> &activation_d_functions)
  : epochs_run(0) {
    if (architecture.size() < 2) {
        throw std::length_error("Architecture must have 2 layers at least.");
    }
    if (architecture.size() - 1 != activation_functions.size()) {
        throw std::length_error(
          "must specify activation function for each layer");
    }
    if (activation_functions.size() != activation_d_functions.size()) {
        throw std::length_error(
          "activations and activations_d must be same size");
    }
    for (size_t m = 1; m < architecture.size(); ++m) {
        layers.push_back(std::vector<Neuron>());
        for (size_t _ = 0; _ < architecture[m]; ++_) {
            layers.back().push_back(Neuron(architecture[m - 1], activation_functions[m - 1], activation_d_functions[m - 1]));
        }
    }
}

std::vector<double> MLN::evaluate(std::vector<double> inputs) {
    std::vector<double> outputs;
    for (auto &layer : layers) {
        outputs.clear();
        for (auto &neuron : layer) {
            outputs.push_back(neuron.evaluate(inputs));
        }
        inputs = outputs;
    }
    return outputs;
}

std::pair<double, size_t> MLN::train(const std::vector<Test> &tests, const double learning_rate, const double max_error, const size_t max_epochs) {
    size_t epochs = 0;
    error = max_error;
    while (error >= max_error and ++epochs < max_epochs) {
        fit(tests, learning_rate);
    }
    return {error, epochs};
}

double MLN::fit(const std::vector<Test> &tests, const double learning_rate) {
    error = 0.0;
    for (const auto &test : tests) {
        evaluate(test.first);
        calculate_sensitivities(test);
        update_weights(test, learning_rate);
    }
    ++epochs_run;
    return error;
}

void MLN::calculate_sensitivities(const Test &test) {
    // s^M
    for (size_t i = 0; i < layers.back().size(); ++i) {
        auto &neuron = layers.back()[i];
        double test_error = test.second[i] - neuron.out;
        error += (test_error * test_error);
        neuron.sensitivity = -2 * neuron.evaluate_d() * test_error;
    }
    error /= 2;
    // s^m
    for (long m_long = static_cast<long>(layers.size()) - 2; m_long >= 0; --m_long) {
        size_t m = static_cast<size_t>(m_long); // Just for beautify code
        for (size_t i = 0; i < layers[m].size(); ++i) {
            layers[m][i].sensitivity = 0;
            for (size_t k = 0; k < layers[m + 1].size(); ++k) {
                layers[m][i].sensitivity += layers[m][i].evaluate_d() * layers[m + 1][k].weights[i + 1] * layers[m + 1][k].sensitivity;
            }
        }
    }
}

void MLN::update_weights(const Test &test, const double learning_rate) {
    auto inputs = test.first;
    for (auto &layer : layers) {
        std::vector<double> outputs;
        inputs.push_back(-1);
        for (auto &neuron : layer) {
            outputs.push_back(neuron.out);
            auto d = -learning_rate * neuron.sensitivity;
            for (size_t w = 0; w < neuron.weights.size(); ++w) {
                neuron.weights[w] += d * inputs[w];
            }
        }
        inputs = outputs;
    }
}

std::vector<std::vector<std::vector<double>>> MLN::weights() const {
    std::vector<std::vector<std::vector<double>>> w;
    for (const auto &layer : layers) {
        std::vector<std::vector<double>> layerWeights;
        for (const auto &neuron : layer) {
            layerWeights.push_back(neuron.weights);
        }
        w.push_back(layerWeights);
    }
    return w;
}
