#include "mainwindow.h"

#include "ui_mainwindow.h"

#include <QButtonGroup>
#include <QSharedPointer>
#include <QString>

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , mln(nullptr) {
    auto xMin = std::get<0>(xAxisRange);
    auto xMax = std::get<1>(xAxisRange);
    auto yMin = std::get<0>(yAxisRange);
    auto yMax = std::get<1>(yAxisRange);

    colors.push_back(QColor::fromRgb(200, 0, 0));
    colors.push_back(QColor::fromRgb(0, 200, 0));
    colors.push_back(QColor::fromRgb(0, 0, 200));
    colors.push_back(QColor::fromRgb(200, 200, 0));
    colors.push_back(QColor::fromRgb(0, 200, 200));
    colors.push_back(QColor::fromRgb(200, 0, 200));

    colors.push_back(QColor::fromRgb(200, 150, 150));
    colors.push_back(QColor::fromRgb(150, 200, 150));
    colors.push_back(QColor::fromRgb(150, 150, 200));
    colors.push_back(QColor::fromRgb(200, 200, 150));
    colors.push_back(QColor::fromRgb(150, 200, 200));
    colors.push_back(QColor::fromRgb(200, 150, 200));

    ui->setupUi(this);

    // Ugly Qt UI things
    ui->groupClasses->layout()->setAlignment(ui->groupClassesGrid, Qt::AlignHCenter);

    // Plot Mode
    plotModeGroup.addButton(ui->firstLayerRadio, 1);
    plotModeGroup.addButton(ui->outputLayerRadio, 2);

    ui->dataPlot->xAxis->setRange(xMin, xMax);
    ui->dataPlot->yAxis->setRange(yMin, yMax);

    // Error Graph
    errorGraph = new QCPBars(ui->errorPlot->xAxis, ui->errorPlot->yAxis);
    ui->errorPlot->xAxis->setLabel("Epoch");
    ui->errorPlot->yAxis->setLabel("Error");
    QSharedPointer<QCPAxisTickerFixed> fixedTicker(new QCPAxisTickerFixed);
    fixedTicker->setTickStep(1.0);
    fixedTicker->setScaleStrategy(QCPAxisTickerFixed::ssMultiples);
    ui->errorPlot->xAxis->setTicker(fixedTicker);

    // Events
    connect(ui->dataPlot, &QCustomPlot::mouseRelease, this, &MainWindow::handlePlotClick);
    connect(&clock, &QTimer::timeout, this, &MainWindow::handleTrainEpoch);
    connect(&plotModeGroup, static_cast<void (QButtonGroup::*)(int)>(&QButtonGroup::buttonClicked), this, &MainWindow::onPlotModeSelected);
}

MainWindow::~MainWindow() {
    ui->errorPlot->removePlottable(errorGraph);
    delete ui;
    delete mln;
}

void MainWindow::handlePlotClick(QMouseEvent *ev) {
    if (mln != nullptr || classGraphs.size() == 0)
        return;
    auto pos = ev->pos();
    auto x = ui->dataPlot->xAxis->pixelToCoord(pos.x());
    auto y = ui->dataPlot->yAxis->pixelToCoord(pos.y());
    if (x >= std::get<0>(xAxisRange) && x <= std::get<1>(xAxisRange) &&
        y >= std::get<0>(yAxisRange) && y <= std::get<1>(yAxisRange)) {
        auto classId = classGroup.checkedId();
        tests.push_back({{x, y}, classId2Vec(classId, classGraphs.size())});
        classGraphs[static_cast<size_t>(classId)]->addData(x, y);
        ui->dataPlot->replot();
        ui->btnInit->setEnabled(true);
    }
}

void MainWindow::handleTrainEpoch() {
    double err = mln->fit(tests, ui->spinLearnRate->value());
    updateState();
    logErr(mln->epochs_run, err);
    if (err < ui->spinTolerance->value() or ui->editEpoch->value() > ui->spinEpochsLimit->value()) {
        clock.stop();
    }
}

void MainWindow::updateState() {
    ui->editEpoch->setValue(static_cast<int>(mln->epochs_run));
    ui->editError->setText(QString::number(mln->error));
    graphResults();
}

void MainWindow::logErr(size_t epoch, double err) {
    errorGraph->addData(epoch, err);
    ui->errorPlot->rescaleAxes();
    ui->errorPlot->replot();
}

void MainWindow::plotState(QCPGraph *graph, const std::vector<double> &w, double th) {
    // ax + by - c = 0 ---> y = (-ax + c) / b
    auto y = [=](double x) { return (-1 * w[0] * x + th) / w[1]; };
    auto xMin = std::get<0>(xAxisRange);
    auto xMax = std::get<1>(xAxisRange);
    graph->setData({xMin, xMax}, {y(xMin), y(xMax)});
    ui->dataPlot->replot();
}

void MainWindow::graphResults() {
    for (auto graph : firstLayerGraphs) {
        graph->setData({}, {});
    }
    for (auto graph : gradientGraphs) {
        graph->data()->clear();
    }
    switch (plotModeGroup.checkedId()) {
        case 1:
            graphFirstLayer();
            break;
        case 2:
            classifySpace();
            break;
    }
}

void MainWindow::graphFirstLayer() {
    auto firstLayerWeights = mln->weights()[0];
    for (size_t i = 0; i < firstLayerWeights.size(); ++i) {
        auto threshold = firstLayerWeights[i][0];
        firstLayerWeights[i].erase(firstLayerWeights[i].begin());
        plotState(firstLayerGraphs[i], firstLayerWeights[i], threshold);
    }
}

void MainWindow::classifySpace() {
    auto xMin = std::get<0>(xAxisRange);
    auto xMax = std::get<1>(xAxisRange);
    auto yMin = std::get<0>(yAxisRange);
    auto yMax = std::get<1>(yAxisRange);
    auto xSize = static_cast<int>((xMax - xMin) / axisStep);
    auto ySize = static_cast<int>((yMax - yMin) / axisStep);
    for (auto graph : gradientGraphs) {
        graph->data()->setSize(xSize, ySize);
        graph->data()->setRange(QCPRange(xMin, xMax), QCPRange(yMin, yMax));
        graph->data()->fillAlpha(0);
    }
    for (int x = 0; x < xSize; ++x) {
        auto xx = xMin + x * axisStep;
        for (int y = 0; y < ySize; ++y) {
            auto yy = yMin + y * axisStep;
            auto result = vec2ClassIdVal(mln->evaluate(std::vector<double>{xx, yy}));
            gradientGraphs[result.first]->data()->setCell(x, y, result.second);
            gradientGraphs[result.first]->data()->setAlpha(x, y, 150);
        }
    }
    ui->dataPlot->replot();
}

void MainWindow::on_spinSearchRangeStart_valueChanged(double arg1) {
    if (ui->spinSearchRangeEnd->value() <= arg1) {
        ui->spinSearchRangeEnd->setValue(arg1 + 0.01);
    }
}

void MainWindow::on_spinSearchRangeEnd_valueChanged(double arg1) {
    if (ui->spinSearchRangeStart->value() >= arg1) {
        ui->spinSearchRangeStart->setValue(arg1 - 0.01);
    }
}

void MainWindow::on_btnInit_clicked() {
    std::vector<size_t> structure;
    structure.push_back(2);
    for (auto neuronsQty : ui->structureLineEdit->text().split(",")) {
        bool conversionOk = true;
        structure.push_back(static_cast<size_t>(neuronsQty.toInt(&conversionOk, 10)));
        if (!conversionOk) {
            QMessageBox::information(nullptr, "Error", "Wrong structure syntax");
            return;
        }
    }
    structure.push_back(classGraphs.size());

    ui->btnInit->setEnabled(false);
    ui->groupClasses->setEnabled(false);
    ui->groupConfig->setEnabled(false);

    mln = new MLN(
      structure,
      std::vector<ActivationFunction>(structure.size() - 1, &MainWindow::logsig),
      std::vector<ActivationFunction>(structure.size() - 1, &MainWindow::logsigDeriv));

    for (size_t i = 0; i < structure[1]; ++i) {
        firstLayerGraphs.push_back(ui->dataPlot->addGraph());
        firstLayerGraphs.back()->setPen(QColor::fromRgb(QRandomGenerator::global()->generate()));
    }

    updateState();
    ui->btnTrain->setEnabled(true);
}

void MainWindow::on_btnTrain_clicked() {
    ui->btnTrain->setEnabled(false);
    ui->btnPause->setEnabled(true);
    clock.start(0);
}

void MainWindow::on_btnPause_clicked() {
    clock.stop();
    ui->btnTrain->setEnabled(true);
    ui->btnPause->setEnabled(false);
}

void MainWindow::on_btnClear_clicked() {
    if (clock.isActive())
        clock.stop();
    for (auto graph : classGraphs) {
        ui->dataPlot->removeGraph(graph);
    }
    for (auto graph : gradientGraphs) {
        graph->data()->clear();
    }
    for (auto graph : firstLayerGraphs) {
        ui->dataPlot->removeGraph(graph);
    }
    classGraphs.clear();
    gradientGraphs.clear();
    firstLayerGraphs.clear();
    errorGraph->setData({}, {});
    ui->dataPlot->replot();
    ui->errorPlot->rescaleAxes();
    ui->errorPlot->yAxis->setRangeLower(0.0);
    ui->errorPlot->replot();

    while (ui->groupClassesGrid->count() > 0) {
        delete ui->groupClassesGrid->itemAt(0)->widget();
    }

    ui->groupClasses->setEnabled(true);
    ui->groupConfig->setEnabled(true);
    ui->editEpoch->clear();
    ui->editStatus->clear();
    ui->editError->clear();
    ui->btnInit->setEnabled(true);
    ui->btnTrain->setEnabled(false);
    ui->btnPause->setEnabled(false);

    tests.clear();
    delete mln;
    mln = nullptr;
}

std::vector<double> MainWindow::classId2Vec(int classId, size_t classesN) {
    std::vector<double> out(classesN, 0.0);
    out[static_cast<size_t>(classId)] = 1.0;
    return out;
}

std::pair<size_t, double> MainWindow::vec2ClassIdVal(std::vector<double> resultVector) {
    double max = resultVector.at(0);
    size_t maxIdx = 0;
    for (size_t i = 0; i < resultVector.size(); ++i) {
        if (resultVector.at(i) > max) {
            max = resultVector.at(i);
            maxIdx = i;
        }
    }
    return std::make_pair(maxIdx, max);
}

void MainWindow::onPlotModeSelected() {
    if (mln != nullptr) {
        graphResults();
    }
}

void MainWindow::on_addClassButton_clicked() {
    if (classGraphs.size() >= 6)
        return;
    QColor color = colors.at(classGraphs.size());
    QColor colorLight = colors.at(classGraphs.size() + colors.size() / 2);

    classGraphs.push_back(ui->dataPlot->addGraph());
    classGraphs.back()->setLineStyle(QCPGraph::lsNone);
    classGraphs.back()->setScatterStyle(QCPScatterStyle(
      QCPScatterStyle::ssCircle, color, 6.0));

    auto grad = QCPColorGradient();
    gradientGraphs.push_back(new QCPColorMap(ui->dataPlot->xAxis, ui->dataPlot->yAxis));
    gradientGraphs.back()->setDataRange(QCPRange(0.0, 1.0));
    gradientGraphs.back()->data()->clear();
    grad.setColorStops({{0.5, colorLight},
                        {1.0, color}});
    gradientGraphs.back()->setGradient(grad);

    QRadioButton *radioButton = new QRadioButton();
    radioButton->setStyleSheet(QString("color:rgb(%1,%2,%3);").arg(color.red()).arg(color.green()).arg(color.blue()));
    radioButton->setText(QString::number(classGraphs.size()));
    classGroup.addButton(radioButton, static_cast<int>(classGraphs.size()) - 1);
    ui->groupClassesGrid->addWidget(radioButton, (static_cast<int>(classGraphs.size()) - 1) / MAX_COLUMNS, (static_cast<int>(classGraphs.size()) - 1) % MAX_COLUMNS);

    if (classGraphs.size() == 1) {
        radioButton->setChecked(true);
    }
}
