#include "neuron.h"

#include <algorithm>
#include <random>

Neuron::Neuron(size_t inputs_n, const std::function<double(double)> &f, const std::function<double(double)> &fd)
  : f(f)
  , fd(fd)
  , weights(inputs_n + 1)
  , net(0) {
    // Initialize state
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> randDistrib(-1, 1);
    std::generate(weights.begin(), weights.end(), [&]() -> double { return randDistrib(gen); });
}

double Neuron::evaluate(std::vector<double> inputs) {
    inputs.push_back(-1);
    if (inputs.size() != weights.size()) {
        throw std::length_error("Inputs size must be equl to weights size.");
    }
    net = 0.0;
    for (size_t i = 0; i < weights.size(); ++i) {
        net += weights[i] * inputs[i];
    }
    out = f(net);
    return out;
}

double Neuron::evaluate_d() {
    return fd(net);
}
