#ifndef NEURON_H
#define NEURON_H

#include <cmath>
#include <functional>
#include <tuple>
#include <vector>

class Neuron {
private:
    std::function<double(double)> f;
    std::function<double(double)> fd;

public:
    std::vector<double> weights;
    double net;
    double out;
    double sensitivity;

    Neuron(size_t inputs_n, const std::function<double(double)> &f, const std::function<double(double)> &fd);

    double evaluate(std::vector<double> inputs);
    double evaluate_d();
};

#endif // NEURON_H
