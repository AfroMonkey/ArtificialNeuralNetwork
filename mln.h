#ifndef ANN_H
#define ANN_H

#include "neuron.h"

#include <vector>

#define ActivationFunction std::function<double(double)>
#define Test std::pair<std::vector<double>, std::vector<double>>

class MLN {
public:
    std::vector<std::vector<Neuron>> layers;
    double error;
    unsigned epochs_run;

    MLN(const std::vector<size_t> &architecture, const std::vector<ActivationFunction> &activation_functions, const std::vector<ActivationFunction> &activation_d_functions);

    std::vector<double> evaluate(std::vector<double> inputs);
    std::pair<double, size_t> train(const std::vector<Test> &tests, const double learning_rate, const double max_error, const size_t max_epochs);
    double fit(const std::vector<Test> &tests, const double learning_rate);
    void calculate_sensitivities(const Test &test);
    void update_weights(const Test &test, const double learning_rate);
    std::vector<std::vector<std::vector<double>>> weights() const;
};

#endif // ANN_H
